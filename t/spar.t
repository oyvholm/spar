#!/usr/bin/env perl

#==============================================================================
# spar.t
# File ID: 7aa6a404-80d8-11e5-a318-02010e0a6634
#
# Test suite for spar(1).
#
# Character set: UTF-8
# ©opyleft 2015– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later, see end of file for 
# legal stuff.
#==============================================================================

use strict;
use warnings;

BEGIN {
    use Test::More qw{no_plan};
    # use_ok() goes here
}

use Getopt::Long;
use IPC::Open3;

local $| = 1;

our $CMD_BASENAME = "spar";
our $CMD = "../$CMD_BASENAME";
my $SQLITE = "sqlite3";

our %Opt = (

    'all' => 0,
    'help' => 0,
    'quiet' => 0,
    'todo' => 0,
    'verbose' => 0,
    'version' => 0,

);

our $progname = $0;
$progname =~ s/^.*\/(.*?)$/$1/;
our $VERSION = '0.0.0';

our $sql_error = 0; # Set to !0 if some sqlite3 error happened
my %descriptions = ();

Getopt::Long::Configure('bundling');
GetOptions(

    'all|a' => \$Opt{'all'},
    'help|h' => \$Opt{'help'},
    'quiet|q+' => \$Opt{'quiet'},
    'todo|t' => \$Opt{'todo'},
    'verbose|v+' => \$Opt{'verbose'},
    'version' => \$Opt{'version'},

) || die("$progname: Option error. Use -h for help.\n");

$Opt{'verbose'} -= $Opt{'quiet'};
$Opt{'help'} && usage(0);
if ($Opt{'version'}) {
    print_version();
    exit(0);
}

chomp(my $dbout_create_config =
    "CREATE TABLE config (\n"
    . "  name TEXT\n"
    . "    CONSTRAINT config_name_length\n"
    . "      CHECK (length(name) > 0)\n"
    . "    UNIQUE\n"
    . "    NOT NULL,\n"
    . "  value JSON\n"
    . "    CONSTRAINT config_value_valid_json\n"
    . "      CHECK (json_valid(value))\n"
    . "    NOT NULL\n"
    . ");\n");
chomp(my $dbout_create_goal =
    "CREATE TABLE goal (\n"
    . "  begindate TEXT\n"
    . "    CONSTRAINT begindate_valid\n"
    . "      CHECK (datetime(begindate) IS NOT NULL)\n"
    . "    NOT NULL,\n"
    . "  beginvalue REAL,\n"
    . "  enddate TEXT\n"
    . "    CONSTRAINT enddate_valid\n"
    . "      CHECK (enddate IS NULL OR datetime(enddate) IS NOT NULL),\n"
    . "  endvalue REAL\n"
    . ");\n");
chomp(my $dbout_create_account =
    "CREATE TABLE account (\n"
    . "  date TEXT\n"
    . "    CONSTRAINT date_valid\n"
    . "      CHECK (datetime(date) IS NOT NULL)\n"
    . "    NOT NULL,\n"
    . "  current REAL\n"
    . ");\n");

exit(main());

sub main {
    my $Retval = 0;

    diag(sprintf('========== Executing %s v%s ==========',
                 $progname, $VERSION));

    if ($Opt{'todo'} && !$Opt{'all'}) {
        goto todo_section;
    }

    my $spardir = "tmp-spar-t-$$-" . substr(rand, 2, 8);
    ok(mkdir($spardir), "mkdir [spardir]");
    ok(chdir($spardir), "chdir [spardir]");
    $CMD = "../$CMD -d .";

    test_standard_options();
    test_missing_db_name();
    test_delete_extension();
    test_nonexisting();
    test_init_args();
    test_empty();
    test_missing_config();
    test_db_too_new();
    test_null();
    test_per_option();
    test_db1();

    ok(chdir(".."), "chdir ..");
    ok(rmdir($spardir), "rmdir [spardir]");

    todo_section:
    ;

    if ($Opt{'all'} || $Opt{'todo'}) {
        diag('Running TODO tests...');
        TODO: {
            local $TODO = '';
            # Insert TODO tests here.
        }
    }

    diag('Testing finished.');

    return $Retval;
}

sub test_standard_options {
    diag('Testing -h (--help) option...');
    likecmd("$CMD -h",
        '/  Show this help/i',
        '/^$/',
        0,
        'Option -h prints help screen');

    diag('Testing -v (--verbose) option...');
    likecmd("$CMD -hv",
        '/^\n\S+ \d+\.\d+\.\d+\b\S*\n/s',
        '/^$/',
        0,
        'Option -v with -h returns version number and help screen');

    diag('Testing --version option...');
    likecmd("$CMD --version",
        '/^\S+ \d+\.\d+\.\d+\b\S*\n/',
        '/^$/',
        0,
        'Option --version returns version number');

    return;
}

sub test_missing_db_name {
    testcmd("$CMD",
        '',
        "No project specified, aborting\n",
        1,
        'Missing project name',
    );

    return;
}

sub test_delete_extension {
    diag("Delete .spar extension if specified");
    create_project(('name' => 'delext'));
    testcmd("$CMD delext",
        "NULL\n",
        "",
        0,
        "dbname without extension");
    testcmd("$CMD delext.spar",
        "NULL\n",
        "",
        0,
        "dbname with extension");
    testcmd("$CMD delext.spar.spar",
        "",
        "spar: delext.spar: Project not found\n",
        1,
        "dbname with two extensions");
    ok(unlink("delext.spar"), "Delete delext.spar");
}

sub test_nonexisting {
    diag("Init database");
    testcmd("$CMD dontexist",
        "",
        "spar: dontexist: Project not found\n",
        1,
        "Abort if project doesn't exist and --init isn't used",
    );

    ok(!-e "dontexist.spar", "dontexist.spar wasn't created");
    testcmd("$CMD --init 'inva lid'",
        "",
        "spar: inva lid: Invalid project name\n",
        1,
        "Reject space in project names",
    );

    return;
}

sub test_init_args {
    diag("--init with args");
    create_project( ('name' => 'init-args', 'now' => '2019-01-02 12:00:00',
                     'args' => "--bt '2019-01-01 12:00:00' --bv=1000"
                               . " --et='2019-01-05 12:00:00' --ev 500 -B",
                     'desc' => "--init with --bt, --bv, --et, --ev and -B",
                     'stdout' => "875.000000\n") );
    is(sqlite_dump("init-args.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "config|below|1\n"
        . "goal|2019-01-01 12:00:00|1000.0|2019-01-05 12:00:00|500.0\n",
        "init-args.spar looks good",
    );
    ok(unlink("init-args.spar"), "Delete init-args.spar");

    return;
}

sub test_empty {
    diag("Empty project");
    create_project( ('name' => 'empty', 'now' => '2015-01-01 00:00:00') );
    is(sqlite_dump("empty.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|NULL|NULL\n",
        "empty.spar looks good 1",
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty",
        "NULL\n",
        "",
        0,
        'After 24 hours, no verbose options',
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty",
        "NULL\n",
        "",
        0,
        'Check 24 hours later',
    );

    is(sql("empty.spar", "DELETE FROM goal;"), "", "Empty goal table");
    is($sql_error, 0, "No $SQLITE error occurred");
    is(sqlite_dump("empty.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n",
        "goal table in empty.spar is empty",
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty",
        "NULL\n",
        "",
        0,
        'Check 24 hours later again',
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty --get-current",
        "NULL\nNULL\n",
        "",
        0,
        'Check with --get-current',
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty -s",
        "NULL\n"
        . "NULL\n"
        . "NULL\n"
        . "NULL, NULL\n",
        "",
        0,
        'Check with -s (--status)',
    );

    testcmd("$CMD --now='2015-01-02 00:00:00' empty -sg",
        "NULL\n"
        . "NULL\n"
        . "NULL\n"
        . "NULL, NULL\n"
        . "ETA NULL, NULL\n",
        "",
        0,
        'Check with -s (--status) and -g',
    );

    testcmd( # Check with --time
        join(" ",
            $CMD,
            "--now='2015-01-02 00:00:00'",
            "--time='2015-01-01 12:00:00'",
            "empty",
            "-s",
        ),
        "Goal at 2015-01-01 12:00:00: NULL\n"
        . "NULL\n"
        . "NULL\n"
        . "NULL, NULL\n",
        "",
        0,
        'Check with --time',
    );

    testcmd( # Use --add in an empty project
        join(" ",
            $CMD,
            "--now='2015-01-02 00:00:00'",
            "--time='2015-01-01 12:00:00'",
            "--add 56",
            "empty",
            "-s",
        ),
        "Goal at 2015-01-01 12:00:00: NULL\n"
        . "56\n"
        . "NULL\n"
        . "NULL, NULL\n",
        "",
        0,
        'Use --add in an empty project',
    );

    is(sqlite_dump("empty.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2015-01-02 00:00:00|56.0\n"
        . "config|dbversion|1\n",
        "Added value is in empty.spar",
    );

    ok(unlink("empty.spar"), "Delete empty.spar");

    return;
}

sub test_missing_config {
    diag("Missing config table");
    create_project( ('name' => 'noconfig', 'now' => '1700-01-01 00:00:00') );
    sql("noconfig.spar", "DROP TABLE config;", "Drop config table");
    is($sql_error, 0, "No $SQLITE error after DROP TABLE config");
    is(sqlite_dump("noconfig.spar"),
        "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "goal|1700-01-01 00:00:00|0.0|NULL|NULL\n",
        "config table is gone from noconfig.spar",
    );

    is($sql_error, 1,
        "$SQLITE error from sqlite_dump() when config table is gone");
    testcmd("$CMD --now='1700-01-02 00:00:00' noconfig -s",
        "",
        "spar: noconfig.spar: config.dbversion not found, cannot continue\n",
        1,
        'Cannot run without config.dbversion',
    );

    is(sqlite_dump("noconfig.spar"),
        "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "goal|1700-01-01 00:00:00|0.0|NULL|NULL\n",
        "config table is still missing in noconfig.spar",
    );

    ok(unlink("noconfig.spar"), "Delete noconfig.spar");

    return;
}

sub test_db_too_new {
    diag("Unsupported database version");
    create_project( ('name' => 'too-new', 'now' => '1400-01-01 00:00:00') );
    sql("too-new.spar",
        "UPDATE config SET value = 2 WHERE name = 'dbversion';",
        "Set dbversion to 2 (unsupported atm)"
    );

    is(sqlite_dump("too-new.spar"),
        "CREATE TABLE config (\n"
        . "  name TEXT\n"
        . "    CONSTRAINT config_name_length\n"
        . "      CHECK (length(name) > 0)\n"
        . "    UNIQUE\n"
        . "    NOT NULL,\n"
        . "  value JSON\n"
        . "    CONSTRAINT config_value_valid_json\n"
        . "      CHECK (json_valid(value))\n"
        . "    NOT NULL\n"
        . ");\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|2\n"
        . "goal|1400-01-01 00:00:00|0.0|NULL|NULL\n",
        "too-new.spar has config.dbversion = 2",
    );

    testcmd("$CMD too-new",
        "",
        "spar: Database version is 2\n"
        . "spar: This version only supports version 1\n",
        1,
        'Trying to read database with unsupported dbversion',
    );

    ok(unlink("too-new.spar"), "Delete too-new.spar");

    return;
}

sub test_null {
    diag("Current value is identical to start value");
    create_project( ('name' => 'null', 'now' => '2019-01-01 12:00:00',
                     'args' => "--bt '2019-01-01 12:00:00' --bv=0"
                               . " --et='2019-01-05 12:00:00' --ev 10 -c 0",
                     'desc' => "--init null with --bt, --bv, --et, --ev",
                     'stdout' => "0.000000\n") );
    testcmd("$CMD --now='2019-01-01 12:00:00' null -sg",
        "0.000000\n"
        . "0.0\n"
        . "0.000000\n"
        . "0d:00:00:00, 2019-01-01 12:00:00\n"
        . "ETA NULL, NULL\n",
        "",
        0,
        'null -sgvv',
    );
    ok(unlink("null.spar"), "Delete null.spar");

    diag("End value is not defined");
    create_project( ('name' => 'null', 'now' => '2019-01-01 12:00:00',
                     'args' => "--bt '2019-01-01 12:00:00' --bv=0"
                               . " --et='2019-01-05 12:00:00' -c 2",
                     'desc' => "--init null without --ev",
                     'stdout' => "NULL\n") );
    testcmd("$CMD --now='2019-01-01 12:00:00' null -sg",
        "NULL\n"
        . "2.0\n"
        . "NULL\n"
        . "NULL, NULL\n"
        . "ETA NULL, NULL\n",
        "",
        0,
        'null -sgvv without end value',
    );
    ok(unlink("null.spar"), "Delete null.spar");

    return;
}

sub test_per_option {
    diag("Test -p/--per option");
    test_per_option_invalid_args();

    diag("-p/--per with valid args");
    create_project( ('name' => 'per', 'now' => '2019-01-01 00:00:00',
                     'args' => "--bt '2019-01-01 00:00:00' --bv=0"
                               . " --ev 9 -c 1 --per 1/day",
                     'stdout' => "0.000000\n") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2019-01-01 00:00:00|1.0\n"
        . "config|dbversion|1\n"
        . "goal|2019-01-01 00:00:00|0.0|2019-01-10 00:00:00|9.0\n",
        "db is ok after init for 1/day");
    testcmd("$CMD per --now='2019-01-02 00:00:00' -sg",
            "1.000000\n"
            . "1.0\n"
            . "0.000000\n"
            . "0d:00:00:00, 2019-01-02 00:00:00\n"
            . "ETA 8d:00:00:00, 2019-01-10 00:00:00\n",
            "",
            0,
            "per.spar has 1/day");
    ok(unlink("per.spar"), "Delete per.spar");

    for my $m ("-", "") {
        diag("--per with " . ($m eq "-" ? "negative" : "positive")
             . " values");
        test_per_option_seconds($m);
        test_per_option_minutes($m);
        test_per_option_hours($m);
        test_per_option_days($m);
        test_per_option_weeks($m);
        test_per_option_months($m);
        test_per_option_years($m);
    }

    return;
}

sub test_per_option_invalid_args {
    diag("-p/--per with invalid args");
    testcmd("$CMD noprog --init -d . --per 4/zz --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Invalid argument in -p/--per option\n",
            1,
            "Unknown unit value in -p/--per argument");
    testcmd("$CMD noprog --init -d . --per 4 --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Invalid argument in -p/--per option\n",
            1,
            "Unit missing in -p/--per");
    testcmd("$CMD noprog --init -d . --per 4/ --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Invalid argument in -p/--per option\n",
            1,
            "Number and slash without unit in -p/--per");
    testcmd("$CMD noprog --init -d . --per /mi --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Invalid argument in -p/--per option\n",
            1,
            "Missing number in -p/--per argument");
    likecmd("$CMD noprog --init -d . --per ./mi --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            '/^$/',
            '/.*Cannot use 0 in -p\/--per\n/',
            1,
            "Single period instead of number in -p/--per argument");
    testcmd("$CMD noprog --init -d . --per 0/hour --bv 97 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Cannot use 0 in -p/--per\n",
            1,
            "0 in --per makes no sense");
    testcmd("$CMD noprog --init -d . --per -5/hour --bv 197 --ev 128"
            . " --bt '2005-03-07 13:45:56'",
            "",
            "$CMD_BASENAME: Negative numbers in -p/--per not allowed\n",
            1,
            "Reject negative numbers in -p/--per");
    ok(!-e "noprog.spar", "noprog.spar doesn't exist");

    return;
}

sub test_per_option_seconds {
    my $m = shift;

    diag("second " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '2021-01-01 12:00:00',
                     'args' => "--bt '2021-01-01 12:00:00'"
                               . " --bv=${m}0 --ev ${m}90"
                               . " -c ${m}0 --per 1/sec",
                     'stdout' => "0.000000\n",
                     'desc' => "--init for ${m}1/second") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2021-01-01 12:00:00|0.0\n"
        . "config|dbversion|1\n"
        . "goal|2021-01-01 12:00:00|0.0|2021-01-01 12:01:30|${m}90.0\n",
        "db after init for ${m}1/second");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_minutes {
    my $m = shift;

    diag("minute " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '2021-01-01 12:00:00',
                     'args' => "--bt '2021-01-01 12:00:00'"
                               . " --bv=${m}13.999 --ev ${m}37.999"
                               . " -c ${m}41.997 --per 0.5/mi",
                     'stdout' => "${m}13.999000\n",
                     'desc' => "--init for ${m}0.5/minute") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2021-01-01 12:00:00|${m}41.997\n"
        . "config|dbversion|1\n"
        . "goal|2021-01-01 12:00:00|${m}13.999|2021-01-01 12:48:00|${m}37.999\n",
        "db after init for ${m}0.5/minute");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_hours {
    my $m = shift;
    my $revm = $m eq "-" ? "" : "-";

    diag("hour " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '1800-01-01 12:00:00',
                     'args' => "--bt '1800-01-01 00:00:00'"
                               . " --bv=${m}0 --ev ${m}24"
                               . " -c ${m}6 -p 0.5/hour",
                     'stdout' => "${m}6.000000\n",
                     'desc' => "--init for ${m}0.5/hour") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|1800-01-01 12:00:00|${m}6.0\n"
        . "config|dbversion|1\n"
        . "goal|1800-01-01 00:00:00|0.0|1800-01-03 00:00:00|${m}24.0\n",
        "db after init for ${m}0.5/hour");
    testcmd("$CMD per --now='1800-01-02 12:00:00' -sg",
            "${m}18.000000\n"
            . "${m}6.0\n"
            . "${revm}12.000000\n"
            . "-1d:00:00:00, 1800-01-01 12:00:00\n"
            . "ETA 4d:12:00:00, 1800-01-07 00:00:00\n",
            "",
            0,
            "per.spar has ${m}0.5/day");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_days {
    my $m = shift;

    diag("day " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '2000-04-07 12:34:56',
                     'args' => "--bt '2000-04-07 12:34:56'"
                               . " --bv=${m}2000 --ev ${m}4000"
                               . " -c ${m}2000 -p 20/day",
                     'stdout' => "${m}2000.000000\n",
                     'desc' => "--init for ${m}20/day") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2000-04-07 12:34:56|${m}2000.0\n"
        . "config|dbversion|1\n"
        . "goal|2000-04-07 12:34:56|${m}2000.0|2000-07-16 12:34:56|${m}4000.0\n",
        "db after init for ${m}20/day");
    likecmd("$CMD per --now='2000-04-07 12:34:56' -sg",
            '/^'
            . "${m}2000\\.000000\\n"
            . "${m}2000\\.0\\n"
            . '0\.000000\n'
            . '0d:00:00:00, 2000-04-07 12:34:56\n'
            . 'ETA NULL, NULL\n'
            . '$/'
            ,
            '/^$/s',
            0,
            "per.spar has ${m}20/day");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_weeks {
    my $m = shift;

    diag("week " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '5001-01-01 00:00:00',
                     'args' => "--bt '5001-01-01 00:00:00'"
                               . " --bv=${m}0"
                               . " --ev ${m}10"
                               . " -c ${m}0"
                               . " -p 0.2/we ",
                     'stdout' => "0.000000\n",
                     'desc' => "--init for ${m}0.2/week") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|5001-01-01 00:00:00|0.0\n"
        . "config|dbversion|1\n"
        . "goal|5001-01-01 00:00:00|0.0|5001-12-17 00:00:00|${m}10.0\n",
        "db after init for ${m}0.2/week");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_months {
    my $m = shift;

    diag("month " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '1901-07-02 15:00:00',
                     'args' => "--bt '1901-01-01 00:00:00'"
                               . " --bv=${m}0"
                               . " --ev ${m}12"
                               . " -c ${m}1"
                               . " -p 1/mo ",
                     'stdout' => "${m}6.000000\n",
                     'desc' => "--init for ${m}1/month") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|1901-07-02 15:00:00|${m}1.0\n"
        . "config|dbversion|1\n"
        . "goal|1901-01-01 00:00:00|0.0|1902-01-01 06:00:00|${m}12.0\n",
        "db is ok after init for ${m}1/month");
    ok(unlink("per.spar"), "Delete per.spar");

    create_project( ('name' => 'per', 'now' => '1349-01-01 13:19:47',
                     'args' => "--bt '1349-01-01 13:19:47'"
                               . " --bv=${m}0"
                               . " --ev ${m}1168.692 " # 97.391 * 12
                               . "-c ${m}194.782 " # 97.391 * 2
                               . "-p 97.391/mo",
                     'stdout' => "0.000000\n",
                     'desc' => "--init for ${m}97.391/month") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|1349-01-01 13:19:47|${m}194.782\n"
        . "config|dbversion|1\n"
        . "goal|1349-01-01 13:19:47|0.0|1350-01-01 19:19:47|${m}1168.692\n",
        "db after init for ${m}97.391/month");
    testcmd("$CMD per --now='1349-01-01 13:19:47' -sg",
            "0.000000\n"
            . "${m}194.782\n"
            . "${m}194.782000\n"
            . "60d:21:00:00, 1349-03-03 10:19:47\n"
            . "ETA 0d:00:00:00, 1349-01-01 13:19:47\n",
            "",
            0,
            "per.spar has ${m}97.391/month");
    testcmd("$CMD per --now='1349-01-03 13:19:47' -sg",
            "${m}6.399409\n"
            . "${m}194.782\n"
            . "${m}188.382591\n"
            . "58d:21:00:00, 1349-03-03 10:19:47\n"
            . "ETA 10d:00:00:00, 1349-01-13 13:19:47\n",
            "",
            0,
            "per.spar has ${m}97.391/month, --now is two days later");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_per_option_years {
    my $m = shift;

    diag("year " . (length($m) ? "down" : "up"));
    create_project( ('name' => 'per', 'now' => '2001-01-03 00:00:00',
                     'args' => "--bt '2001-01-01 00:00:00'"
                               . " --bv=${m}0 --ev ${m}365.25"
                               . " -c ${m}18.5 -p 365.25/ye",
                     'stdout' => "${m}2.000000\n",
                     'desc' => "--init for ${m}365.25/year") );
    is(sqlite_dump("per.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2001-01-03 00:00:00|${m}18.5\n"
        . "config|dbversion|1\n"
        . "goal|2001-01-01 00:00:00|0.0|2002-01-01 06:00:00|${m}365.25\n",
        "db after init for ${m}365.25/year");
    testcmd("$CMD per --now='2001-01-19 12:00:00' -sg",
            "${m}18.500000\n"
            . "${m}18.5\n"
            . "0.000000\n"
            . "0d:00:00:00, 2001-01-19 12:00:00\n"
            . "ETA 346d:18:00:00, 2002-01-01 06:00:00\n",
            "",
            0,
            "per.spar has ${m}365.25/year");
    ok(unlink("per.spar"), "Delete per.spar");

    return;
}

sub test_db1 {
    diag("Various tests on db1.spar");
    create_project( ('name' => 'db1', 'now' => '2015-01-01 00:00:00') );
    testcmd("$CMD --now='2015-01-32 00:00:00' db1",
        '',
        "spar: '2015-01-32 00:00:00': Invalid --now timestamp\n",
        1,
        'Reject invalid --now date',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|NULL|NULL\n",
        "db1 looks good 1",
    );

    test_db1_ev_and_et_option();
    test_db1_bv_and_bt_option();
    test_db1_1_year_goal();
    test_db1_current_option();
    test_db1_above_and_below_option();
    test_db1_get_current_option();
    test_db1_status_option_and_terminal();
    test_db1_when_option();
    test_db1_add_option();
    test_db1_down_goal();
    ok(unlink("db1.spar"), "Delete db1.spar");

    return;
}

sub test_db1_ev_and_et_option {
    diag("--ev/--end-value and --et/--end-time");
    testcmd("$CMD db1 --now='2015-01-01 12:00' --ev=86400 --et='2015-01-02 00:00:00'",
        "43200.000000\n",
        "",
        0,
        '--ev=86400, --et 24 hours later',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|2015-01-02 00:00:00|86400.0\n",
        "db1 looks good 2",
    );

    testcmd("$CMD db1 --now='2015-01-01 18:00:00' --end-value=240 --end-time='2015-01-02 12:00:00'",
        "120.000000\n",
        "",
        0,
        '--end-value and --end-time',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|2015-01-02 12:00:00|240.0\n",
        "db1 looks good 3",
    );

    return;
}

sub test_db1_bv_and_bt_option {
    diag("--bv/--begin-value and --bt/--begin-time");
    testcmd("$CMD db1 --now='2015-01-01 03:00:00' --bv=13.713191 --bt='2015-01-01 03:00:00'",
        "13.713191\n",
        "",
        0,
        '--bv and --bt',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 03:00:00|13.713191|2015-01-02 12:00:00|240.0\n",
        "db1 looks good 4",
    );

    testcmd("$CMD db1 --now='2015-01-01 09:00:00' --begin-value=1800 --begin-time='2015-01-02 06:00:00'",
        "7260.000000\n",
        "",
        0,
        '--begin-value and --begin-time',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-02 06:00:00|1800.0|2015-01-02 12:00:00|240.0\n",
        "db1 looks good 5",
    );

    return;
}

sub test_db1_1_year_goal {
    diag("Set up 1 year goal span");
    testcmd( # Set 1 year goal span
        join(" ",
            $CMD,
            "db1",
            "--now='2016-01-01 00:00:00'",
            "--bt='2015-01-01 00:00:00'",
            "--bv=0",
            "--et='2016-01-01 00:00:00'",
            "--ev=31536000",
        ),
        "31536000.000000\n",
        "",
        0,
        'Set 1 year goal span',
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|2016-01-01 00:00:00|31536000.0\n",
        "db1 looks good 6",
    );

    diag("Middle of year");
    testcmd("$CMD --now='2015-07-02 12:00:00' db1",
        "15768000.000000\n",
        "",
        0,
        "Goal at middle of year",
    );

    testcmd("$CMD --now='2015-07-02 12:00:00' db1",
        "15768000.000000\n",
        "",
        0,
        "Goal at middle of year with -v --verbose",
    );

    testcmd("$CMD --time='2015-07-02 12:00:00' --now='2015-01-01' db1",
        "Goal at 2015-07-02 12:00:00: 15768000.000000\n",
        "",
        0,
        "Goal at middle of year with --time -v --verbose",
    );

    return;
}

sub test_db1_current_option {
    diag("-c/--current");
    testcmd("$CMD --now='2015-01-02 00:00:00' -c 86400 db1",
        "86400.000000\n",
        "",
        0,
        "-c 86400",
    );

    testcmd("$CMD --now='2015-01-21 00:00:00' --current 864000 db1",
        "1728000.000000\n",
        "",
        0,
        "--current 864000",
    );

    is(sqlite_dump("db1.spar"),
        "$dbout_create_config\n"
        . "$dbout_create_goal\n"
        . "$dbout_create_account\n"
        . "account|2015-01-02 00:00:00|86400.0\n"
        . "account|2015-01-21 00:00:00|864000.0\n"
        . "config|dbversion|1\n"
        . "goal|2015-01-01 00:00:00|0.0|2016-01-01 00:00:00|31536000.0\n",
        "db1 after -c 864000",
    );

    return;
}

sub test_db1_status_option_and_terminal {
    diag("Terminal support");
    my $term = "xterm";
    my $t_bold = `tput -T$term bold`;
    my $t_green = `tput -T$term setaf 2`;
    my $t_red = `tput -T$term setaf 1`;
    my $t_reset = `tput -T$term sgr0`;
    ok(length($t_bold), "Terminal supports bold");
    ok(length($t_red), "Terminal supports red");
    ok(length($t_green), "Terminal supports green");
    ok(length($t_reset), "Terminal supports reset");

    diag("-s/--status");
    testcmd("$CMD --now='2015-02-07 18:00:00' db1 -s -g",
        join("\n",
            "3261600.000000",
            "864000.0",
            "-2397600.000000",
            "-27d:18:00:00, 2015-01-11 00:00:00",
            "ETA 1340d:03:00:00, 2018-10-09 21:00:00",
            "",
        ),
        "",
        0,
        "-s option",
    );

    testcmd("TERM=$term $CMD --now='2015-02-07 18:00:00' db1 -sg --colour",
        join("\n",
            "3261600.000000",
            "864000.0",
            "$t_bold$t_red-2397600.000000$t_reset",
            "$t_bold$t_red-27d:18:00:00$t_reset, 2015-01-11 00:00:00",
            "ETA 1340d:03:00:00, 2018-10-09 21:00:00",
            "",
        ),
        "",
        0,
        "-s option with --colour, negative is red",
    );

    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --status --goal",
        join("\n",
            "10368000.000000",
            "864000.0",
            "-9504000.000000",
            "-110d:00:00:00, 2015-01-11 00:00:00",
            "ETA 4260d:00:00:00, 2026-12-29 00:00:00",
            "",
        ),
        "",
        0,
        "--status",
    );

    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --status",
        join("\n",
            "10368000.000000",
            "864000.0",
            "-9504000.000000",
            "-110d:00:00:00, 2015-01-11 00:00:00",
            "",
        ),
        "",
        0,
        "--status without --goal",
    );

    testcmd("$CMD --now='2015-05-01 00:00:00' db1 -c 10368000 --status --goal",
        join("\n",
            "10368000.000000",
            "10368000.0",
            "0.000000",
            "0d:00:00:00, 2015-05-01 00:00:00",
            "ETA 245d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Set current value to goal value",
    );

    testcmd("TERM=$term $CMD --now='2015-05-01 00:00:00' db1 -sg",
        join("\n",
            "10368000.000000",
            "10368000.0",
            "0.000000",
            "0d:00:00:00, 2015-05-01 00:00:00",
            "ETA 245d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Use --colour, but zero is neutral",
    );

    testcmd("TERM=$term $CMD --now='2015-04-30 00:00:00' db1 -sg --colour",
        join("\n",
            "10281600.000000",
            "10368000.0",
            "$t_bold${t_green}86400.000000$t_reset",
            "$t_bold${t_green}1d:00:00:00$t_reset, 2015-05-01 00:00:00",
            "ETA 242d:23:00:00, 2015-12-28 23:00:00",
            "",
        ),
        "",
        0,
        "One day earlier is green",
    );

    testcmd("TERM=$term $CMD --now='2015-04-30 00:00:00' db1 -sg --colour --no-colour",
        join("\n",
            "10281600.000000",
            "10368000.0",
            "86400.000000",
            "1d:00:00:00, 2015-05-01 00:00:00",
            "ETA 242d:23:00:00, 2015-12-28 23:00:00",
            "",
        ),
        "",
        0,
        "--no-colour turns colours off",
    );

    testcmd("$CMD --now='2016-01-01 00:00:00' db1 -c 31536000 --status --goal",
        join("\n",
            "31536000.000000",
            "31536000.0",
            "0.000000",
            "0d:00:00:00, 2016-01-01 00:00:00",
            "ETA 0d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Goal is reached",
    );

    testcmd("$CMD --now='2016-01-02 00:00:00' db1 --status",
        join("\n",
            "31622400.000000",
            "31536000.0",
            "-86400.000000",
            "-1d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Goal is passed, current value is unchanged",
    );

    testcmd("$CMD --now='2016-01-02 00:00:00' -g -c 31622400 db1 --status",
        join("\n",
            "31622400.000000",
            "31622400.0",
            "0.000000",
            "0d:00:00:00, 2016-01-02 00:00:00",
            "ETA -1d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Goal is passed, current value is up to date",
    );

    return;
}

sub test_db1_above_and_below_option {
    diag("-A/--above and -B/--below");
    for my $p (0, 1) {
        my $o_a = $p ? "-A" : "--above";
        my $o_b = $p ? "-B" : "--below";
        testcmd("$CMD $o_a $o_b db1",
            "",
            "spar: Cannot combine -A/--above and -B/--below\n",
            1,
            "$o_a and $o_b together is not allowed",
        );

        testcmd("$CMD --now='2015-01-21 00:00:00' $o_b db1",
            "1728000.000000\n",
            "",
            0,
            "Use $o_b",
        );

        is(sqlite_dump("db1.spar"),
            "$dbout_create_config\n"
            . "$dbout_create_goal\n"
            . "$dbout_create_account\n"
            . "account|2015-01-02 00:00:00|86400.0\n"
            . "account|2015-01-21 00:00:00|864000.0\n"
            . "config|dbversion|1\n"
            . "config|below|1\n"
            . "goal|2015-01-01 00:00:00|0.0|2016-01-01 00:00:00|31536000.0\n",
            "db1 has config.below = 1 after $o_b",
        );

        testcmd("$CMD --now='2015-01-21 00:00:00' $o_a db1",
            "1728000.000000\n",
            "",
            0,
            "Use $o_a",
        );

        is(sqlite_dump("db1.spar"),
            "$dbout_create_config\n"
            . "$dbout_create_goal\n"
            . "$dbout_create_account\n"
            . "account|2015-01-02 00:00:00|86400.0\n"
            . "account|2015-01-21 00:00:00|864000.0\n"
            . "config|dbversion|1\n"
            . "goal|2015-01-01 00:00:00|0.0|2016-01-01 00:00:00|31536000.0\n",
            "config.below is gone after $o_a",
        );
    }

    return;
}

sub test_db1_get_current_option {
    diag("--get-current");
    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --get-current",
        join("\n",
            "10368000.000000",
            "864000.0",
            "",
        ),
        "",
        0,
        "--get-current",
    );

    return;
}

sub test_db1_when_option {
    diag("-w/--when");
    create_project( ('name' => 'when', 'now' => '1900-01-01 00:00:00') );
    testcmd( # Init when.spar
        join(" ",
            $CMD,
            "when",
            "--now='1900-01-03 12:00:00'",
            "--bv=2",
            "--bt='1900-01-01 00:00:00'",
            "--ev=20",
            "--et='1900-01-10 00:00:00'",
        ),
        "7.000000\n",
        "",
        0,
        "Init when.spar",
    );

    testcmd( # Print time when current goal is 13
        join(" ",
            $CMD,
            "when",
            "--now='1900-01-02 00:00:00'",
            "--when=13",
        ),
        "4d:12:00:00, 1900-01-06 12:00:00\n",
        "",
        0,
        "Print time when current goal is 13",
    );

    testcmd( # Init values together with --when
        join(" ",
            $CMD,
            "when",
            "--now='1800-05-27 00:00:00'",
            "--bv=10.75",
            "--bt='1800-05-10 18:00:00'",
            "--ev=29",
            "--et='1800-05-29 00:00:00'",
            "--when=28.25",
        ),
        "1d:06:00:00, 1800-05-28 06:00:00\n",
        "",
        0,
        "Init values together with --when",
    );

    testcmd( # Goal in the past
        join(" ",
            $CMD,
            "when",
            "--now='1800-05-12 00:00:00'",
            "--bv=10.75",
            "--bt='1800-05-10 18:00:00'",
            "--ev=29",
            "--et='1800-05-29 00:00:00'",
            "--when=3.5",
        ),
        "-8d:12:00:00, 1800-05-03 12:00:00\n",
        "",
        0,
        "Goal in the past",
    );

    testcmd( # Negative --when
        join(" ",
            $CMD,
            "when",
            "--now='1100-04-24 12:00:00'",
            "--bv=20",
            "--bt='1100-05-20 00:00:00'",
            "--ev=30",
            "--et='1100-05-30 00:00:00'",
            "--when=-5.5",
        ),
        "0d:00:00:00, 1100-04-24 12:00:00\n",
        "",
        0,
        "Negative --when",
    );
    ok(unlink("when.spar"), "Delete when.spar");

    create_project( ('name' => 'when-missing',
        'now' => '1900-01-01 00:00:00') );
    testcmd( # All values are missing
        join(" ",
            $CMD,
            "when-missing",
            "--when=5",
        ),
        "",
        "spar: Missing begin/end time or begin/end value\n",
        1,
        "All values are missing",
    );

    for my $curr_invmix ("--goal", "--status", "--time='1900-01-01 12:00'") {
        testcmd( # Cannot combine --when and $curr_invmix
            join(" ",
                $CMD,
                "when-missing",
                "$curr_invmix",
                "--when=5",
            ),
            "",
            "spar: Cannot combine --when with --goal, --status or --time\n",
            1,
            "Cannot combine --when and $curr_invmix",
        );
    }
    ok(unlink("when-missing.spar"), "Delete when-missing.spar");

    return;
}

sub test_db1_add_option {
    diag("-a/--add");
    testcmd("$CMD --now='2016-01-03 00:00:00' -g -a 86400 db1 --status",
        join("\n",
            "31708800.000000",
            "31708800",
            "0.000000",
            "0d:00:00:00, 2016-01-03 00:00:00",
            "ETA -2d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "-a 86400",
    );

    testcmd("$CMD --now='2016-01-04 00:00:00' --add -43200 db1 --status",
        join("\n",
            "31795200.000000",
            "31665600",
            "-129600.000000",
            "-1d:12:00:00, 2016-01-02 12:00:00",
            "",
        ),
        "",
        0,
        "--add with negative number",
    );

    return;
}

sub test_db1_down_goal {
    diag("Goal goes down instead of up");
    create_project( ('name' => 'down', 'now' => '2003-01-01 00:00:00') );
    testcmd( # Init downwards goal
        join(" ",
            $CMD,
            "down",
            "--now='2003-01-11 00:00:00'",
            "--bv=365",
            "--et='2004-01-01 00:00:00'",
            "--ev=0",
            "-c 351",
            "-sg",
        ),
        "355.000000\n"
        . "351.0\n"
        . "-4.000000\n"
        . "4d:00:00:00, 2003-01-15 00:00:00\n"
        . "ETA 250d:17:08:34, 2003-09-18 17:08:34\n"
        . "",
        "",
        0,
        'Init downwards goal',
    );

    testcmd( # --goal
        join(" ",
            $CMD,
            "--goal",
            "down",
            "--now='2003-01-12 00:00:00'",
            "-c 355.5",
            "-s",
        ),
        "354.000000\n"
        . "355.5\n"
        . "1.500000\n"
        . "-1d:12:00:00, 2003-01-10 12:00:00\n"
        . "ETA 411d:15:09:28, 2004-02-27 15:09:28\n"
        . "",
        "",
        0,
        'Downwards goal and ahead',
    );

    likecmd("$CMD down -B", '/.*/', '/^$/', 0, "down -B");
    testcmd( # --goal, below
        join(" ",
            $CMD,
            "--goal",
            "down",
            "--now='2003-01-12 00:00:00'",
            "-c 355.5",
            "-s",
        ),
        "354.000000\n"
        . "355.5\n"
        . "-1.500000\n"
        . "-1d:12:00:00, 2003-01-10 12:00:00\n"
        . "ETA 411d:15:09:28, 2004-02-27 15:09:28\n"
        . "",
        "",
        0,
        'Downwards, -B turns it into behind',
    );

    likecmd("$CMD down --above", '/.*/', '/^$/', 0, "down --above");
    testcmd( # --goal, above
        join(" ",
            $CMD,
            "--goal",
            "down",
            "--now='2003-01-12 00:00:00'",
            "-c 355.5",
            "-s",
        ),
        "354.000000\n"
        . "355.5\n"
        . "1.500000\n"
        . "-1d:12:00:00, 2003-01-10 12:00:00\n"
        . "ETA 411d:15:09:28, 2004-02-27 15:09:28\n"
        . "",
        "",
        0,
        'Downwards, is ahead again',
    );
    ok(unlink("down.spar"), "Delete down.spar");

    return;
}

sub create_project {
    my %in = @_;
    my $args_str = '';
    my $name_str = '';
    my $now_str = '';
    my $stdout_str = "NULL\n";

    if (defined($in{'now'})) {
        $now_str = " --now='$in{'now'}'";
    }
    if (defined($in{'args'})) {
        $args_str = " $in{'args'}";
    }
    if (defined($in{'name'})) {
        $name_str = "$in{'name'}";
    } else {
        BAIL_OUT("create_project(): \"name\" element not specified");
    }
    my $desc_str = defined($in{'desc'})
                       ? $in{'desc'}
                       : "--init $name_str";
    if (defined($in{'stdout'})) {
        $stdout_str = "$in{'stdout'}";
    }
    my $retval = testcmd("$CMD$now_str --init$args_str $name_str",
        $stdout_str,
        "spar: Initialising database ./$name_str.spar\n",
        0,
        $desc_str,
    );

    return $retval;
}

sub sql {
    my ($db, $sql) = @_;
    my @retval = ();

    msg(5, "sql(): db = '$db'");
    local(*CHLD_IN, *CHLD_OUT, *CHLD_ERR);

    $sql_error = 0;
    my $pid = open3(*CHLD_IN, *CHLD_OUT, *CHLD_ERR, $SQLITE, $db) or (
        $sql_error = 1,
        msg(0, "sql(): open3() error: $!"),
        return "sql() error",
    );
    msg(5, "sql(): sql = '$sql'");
    print(CHLD_IN "$sql\n") or msg(0, "sql(): print CHLD_IN error: $!");
    close(CHLD_IN);
    @retval = <CHLD_OUT>;
    msg(5, "sql(): retval = '" . join('|', @retval) . "'");
    my @child_stderr = <CHLD_ERR>;
    if (scalar(@child_stderr)) {
        msg(1, "$SQLITE error: " . join('', @child_stderr));
        $sql_error = 1;
    }

    return join('', @retval);
}

sub sqlite_dump {
    # Return contents of database file
    my $File = shift;

    return sql($File,
        ".nullvalue NULL\n"
        . ".schema\n"
        . "SELECT 'account', * FROM account;\n"
        . "SELECT 'config', * FROM config;\n"
        . "SELECT 'goal', * FROM goal;\n"
    );
}

sub testcmd {
    my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
    defined($descriptions{$Desc})
    && BAIL_OUT("testcmd(): '$Desc' description is used twice");
    $descriptions{$Desc} = 1;
    my $stderr_cmd = '';
    my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
    my $Txt = join('', $cmd_outp_str, defined($Desc) ? $Desc : '');
    my $TMP_STDERR = "$CMD_BASENAME-stderr.tmp";
    my $retval = 1;

    if (defined($Exp_stderr)) {
        $stderr_cmd = " 2>$TMP_STDERR";
    }
    $retval &= is(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
    my $ret_val = $?;
    if (defined($Exp_stderr)) {
        $retval &= is(file_data($TMP_STDERR), $Exp_stderr, "$Txt (stderr)");
        unlink($TMP_STDERR);
    } else {
        diag("Warning: stderr not defined for '$Txt'");
    }
    $retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");

    return $retval;
}

sub likecmd {
    my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
    defined($descriptions{$Desc})
    && BAIL_OUT("likecmd(): '$Desc' description is used twice");
    $descriptions{$Desc} = 1;
    my $stderr_cmd = '';
    my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
    my $Txt = join('', $cmd_outp_str, defined($Desc) ? $Desc : '');
    my $TMP_STDERR = "$CMD_BASENAME-stderr.tmp";
    my $retval = 1;

    if (defined($Exp_stderr)) {
        $stderr_cmd = " 2>$TMP_STDERR";
    }
    $retval &= like(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
    my $ret_val = $?;
    if (defined($Exp_stderr)) {
        $retval &= like(file_data($TMP_STDERR), $Exp_stderr, "$Txt (stderr)");
        unlink($TMP_STDERR);
    } else {
        diag("Warning: stderr not defined for '$Txt'");
    }
    $retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");

    return $retval;
}

sub file_data {
    # Return file content as a string
    my $File = shift;
    my $Txt;

    open(my $fp, '<', $File) or return undef;
    local $/ = undef;
    $Txt = <$fp>;
    close($fp);

    return $Txt;
}

sub print_version {
    # Print program version
    print("$progname $VERSION\n");

    return;
}

sub usage {
    # Send the help message to stdout
    my $Retval = shift;

    if ($Opt{'verbose'}) {
        print("\n");
        print_version();
    }
    print(<<"END");

Usage: $progname [options]

Contains tests for the $CMD_BASENAME(1) program.

Options:

  -a, --all
    Run all tests, also TODOs.
  -h, --help
    Show this help.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -t, --todo
    Run only the TODO tests.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  --version
    Print version information.

END
    exit($Retval);
}

sub msg {
    # Print a status message to stderr based on verbosity level
    my ($verbose_level, $Txt) = @_;

    $verbose_level > $Opt{'verbose'} && return;
    print(STDERR "$progname: $Txt\n");

    return;
}

__END__

# This program is free software; you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation; either version 2 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program.
# If not, see L<http://www.gnu.org/licenses/>.

# vim: set fenc=UTF-8 ft=perl ts=4 sw=4 sts=4 et fo+=w :
