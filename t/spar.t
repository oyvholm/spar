#!/usr/bin/env perl

#=======================================================================
# spar.t
# File ID: 7aa6a404-80d8-11e5-a318-02010e0a6634
#
# Test suite for spar(1).
#
# Character set: UTF-8
# ©opyleft 2015– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later, see end of 
# file for legal stuff.
#=======================================================================

use strict;
use warnings;

BEGIN {
    use Test::More qw{no_plan};
    # use_ok() goes here
}

use Getopt::Long;
use IPC::Open3;

local $| = 1;

our $CMD_BASENAME = "spar";
our $CMD = "../$CMD_BASENAME";
my $SQLITE = "sqlite3";

our %Opt = (

    'all' => 0,
    'help' => 0,
    'quiet' => 0,
    'todo' => 0,
    'verbose' => 0,
    'version' => 0,

);

our $progname = $0;
$progname =~ s/^.*\/(.*?)$/$1/;
our $VERSION = '0.3.0+git';

my %descriptions = ();

Getopt::Long::Configure('bundling');
GetOptions(

    'all|a' => \$Opt{'all'},
    'help|h' => \$Opt{'help'},
    'quiet|q+' => \$Opt{'quiet'},
    'todo|t' => \$Opt{'todo'},
    'verbose|v+' => \$Opt{'verbose'},
    'version' => \$Opt{'version'},

) || die("$progname: Option error. Use -h for help.\n");

$Opt{'verbose'} -= $Opt{'quiet'};
$Opt{'help'} && usage(0);
if ($Opt{'version'}) {
    print_version();
    exit(0);
}

my $sql_error = 0;

exit(main());

sub main {
    # {{{
    my $Retval = 0;

    diag(sprintf('========== Executing %s v%s ==========',
        $progname,
        $VERSION));

    if ($Opt{'todo'} && !$Opt{'all'}) {
        goto todo_section;
    }

=pod

    testcmd("$CMD command", # {{{
        <<'END',
[expected stdout]
END
        '',
        0,
        'description',
    );

    # }}}

=cut

    diag('Testing -h (--help) option...');
    likecmd("$CMD -h", # {{{
        '/  Show this help/i',
        '/^$/',
        0,
        'Option -h prints help screen',
    );

    # }}}
    diag('Testing -v (--verbose) option...');
    likecmd("$CMD -hv", # {{{
        '/^\n\S+ \d+\.\d+\.\d+\b\S*\n/s',
        '/^$/',
        0,
        'Option -v with -h returns version number and help screen',
    );

    # }}}
    diag('Testing --version option...');
    likecmd("$CMD --version", # {{{
        '/^\S+ \d+\.\d+\.\d+\b\S*\n/',
        '/^$/',
        0,
        'Option --version returns version number',
    );

    # }}}
    testcmd("$CMD", # {{{
        '',
        "No project specified, aborting\n",
        1,
        'Missing project name',
    );

    # }}}
    my $spardir = "tmp-spar-t-$$-" . substr(rand, 2, 8);
    ok(mkdir($spardir), "mkdir [spardir]");
    ok(chdir($spardir), "chdir [spardir]");
    $CMD = "../$CMD -d .";
    chomp(my $dbout_top = <<'END'); # {{{
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
END
    chomp(my $dbout_create_config = <<'END');
CREATE TABLE config (
  name TEXT
    CONSTRAINT config_name_length
      CHECK (length(name) > 0)
    UNIQUE
    NOT NULL
  ,
  value JSON
    CONSTRAINT config_value_valid_json
      CHECK (json_valid(value))
    NOT NULL
);
INSERT INTO "config" VALUES('dbversion',1);
END
chomp(my $dbout_create_goal = <<'END');
CREATE TABLE goal (
  begindate TEXT
    CONSTRAINT begindate_valid
      CHECK (datetime(begindate) IS NOT NULL)
    NOT NULL
  ,
  beginvalue REAL
  ,
  enddate TEXT
    CONSTRAINT enddate_valid
      CHECK (enddate IS NULL OR datetime(enddate) IS NOT NULL)
  ,
  endvalue REAL
);
END

    # }}}
    chomp(my $dbout_create_account = <<'END'); # {{{
CREATE TABLE account (
  date TEXT
    CONSTRAINT date_valid
      CHECK (datetime(date) IS NOT NULL)
    NOT NULL
  ,
  current REAL
);
END

    # }}}
    diag("Init database");
    testcmd("$CMD dontexist", # {{{
        "",
        "spar: dontexist: Project not found\n",
        1,
        "Abort if project doesn't exist and --init isn't used",
    );

    # }}}
    ok(!-e "dontexist.spar", "dontexist.spar wasn't created");
    testcmd("$CMD --init 'inva lid'", # {{{
        "",
        "spar: inva lid: Invalid project name\n",
        1,
        "Reject space in project names",
    );

    # }}}
    create_project( ('name' => 'empty', 'now' => '2015-01-01 00:00:00') );
    is(sqlite_dump("empty.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,NULL,NULL);
$dbout_create_account
COMMIT;
END
        "empty.spar looks good 1",
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty", # {{{
        "NULL\n",
        "",
        0,
        'After 24 hours, no verbose options',
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty -vv", # {{{
        "NULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check 24 hours later',
    );

    # }}}
    is(sql("empty.spar", "DELETE FROM goal;"), "", "Empty goal table");
    is($sql_error, 0, "No $SQLITE error occurred");
    is(sqlite_dump("empty.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
$dbout_create_account
COMMIT;
END
        "goal table in empty.spar is empty",
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty -vv", # {{{
        "NULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check 24 hours later again',
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty --get-current -vv", # {{{
        "NULL\nNULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check with --get-current',
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty -s -vv", # {{{
        "NULL\nNULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check with -s (--status)',
    );

    # }}}
    testcmd("$CMD --now='2015-01-02 00:00:00' empty -sg -vv", # {{{
        "NULL\nNULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check with -s (--status) and -g',
    );

    # }}}
    testcmd( # Check with --time {{{
        join(" ",
            $CMD,
            "--now='2015-01-02 00:00:00'",
            "--time='2015-01-01 12:00:00'",
            "empty",
            "-s",
            "-vv",
        ),
        "Goal at 2015-01-01 12:00:00: NULL\nNULL\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is ''
END
        0,
        'Check with --time',
    );

    # }}}
    testcmd( # Use --add in an empty project {{{
        join(" ",
            $CMD,
            "--now='2015-01-02 00:00:00'",
            "--time='2015-01-01 12:00:00'",
            "--add 56",
            "empty",
            "-s",
            "-vv",
        ),
        "Goal at 2015-01-01 12:00:00: NULL\n56\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started  with value 
spar: Goal ends at  with value 
spar: Current value is '56'
END
        0,
        'Use --add in an empty project',
    );

    # }}}
    is(sqlite_dump("empty.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
$dbout_create_account
INSERT INTO "account" VALUES('2015-01-02 00:00:00',56.0);
COMMIT;
END
        "Added value is in empty.spar",
    );

    # }}}
    ok(unlink("empty.spar"), "Delete empty.spar");
    diag("Old style project without config table");
    create_project( ('name' => 'noconfig', 'now' => '1700-01-01 00:00:00') );
    sql("noconfig.spar", "DROP TABLE config;", "Drop config table");
    is($sql_error, 0, "No $SQLITE error after DROP TABLE config");
    is(sqlite_dump("noconfig.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_goal
INSERT INTO "goal" VALUES('1700-01-01 00:00:00',0.0,NULL,NULL);
$dbout_create_account
COMMIT;
END
        "config table is gone from noconfig.spar",
    );

    # }}}
    testcmd("$CMD --now='1700-01-02 00:00:00' noconfig -s", # {{{
        "NULL\nNULL\n",
        <<END,
spar: noconfig.spar: config table not found, creating it
END
        0,
        'spar noconfig, config table is created',
    );

    # }}}
    is(sqlite_dump("noconfig.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_goal
INSERT INTO "goal" VALUES('1700-01-01 00:00:00',0.0,NULL,NULL);
$dbout_create_account
$dbout_create_config
COMMIT;
END
        "config table is created in noconfig.spar",
    );

    # }}}
    ok(unlink("noconfig.spar"), "Delete noconfig.spar");
    create_project( ('name' => 'too-new', 'now' => '1400-01-01 00:00:00') );
    sql("too-new.spar", # {{{
        "UPDATE config SET value = 2 WHERE name = 'dbversion';",
        "Set dbversion to 2 (unsupported atm)"
    );

    # }}}
    is($sql_error, 0, "No $SQLITE error after update of dbversion");
    is(sqlite_dump("too-new.spar"), # {{{
        <<END,
$dbout_top
CREATE TABLE config (
  name TEXT
    CONSTRAINT config_name_length
      CHECK (length(name) > 0)
    UNIQUE
    NOT NULL
  ,
  value JSON
    CONSTRAINT config_value_valid_json
      CHECK (json_valid(value))
    NOT NULL
);
INSERT INTO "config" VALUES('dbversion',2);
$dbout_create_goal
INSERT INTO "goal" VALUES('1400-01-01 00:00:00',0.0,NULL,NULL);
$dbout_create_account
COMMIT;
END
        "too-new.spar has config.dbversion = 2",
    );

    # }}}
    testcmd("$CMD too-new", # {{{
        "",
        <<END,
spar: Database version is 2
spar: This version only supports version 1
END
        1,
        'Trying to read database with unsupported dbversion',
    );

    # }}}
    ok(unlink("too-new.spar"), "Delete too-new.spar");
    create_project( ('name' => 'db1', 'now' => '2015-01-01 00:00:00') );
    testcmd("$CMD --now='2015-01-32 00:00:00' db1", # {{{
        '',
        "spar: '2015-01-32 00:00:00': Invalid --now timestamp\n",
        1,
        'Reject invalid --now date',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,NULL,NULL);
$dbout_create_account
COMMIT;
END
        "db1 looks good 1",
    );

    # }}}
    diag("--ev/--end-value and --et/--end-time");
    testcmd("$CMD db1 --now='2015-01-01 12:00' --ev=86400 --et='2015-01-02 00:00:00'", # {{{
        "43200.000000\n",
        "",
        0,
        '--ev=86400, --et 24 hours later',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,'2015-01-02 00:00:00',86400.0);
$dbout_create_account
COMMIT;
END
        "db1 looks good 2",
    );

    # }}}
    testcmd("$CMD db1 --now='2015-01-01 18:00:00' --end-value=240 --end-time='2015-01-02 12:00:00'", # {{{
        "120.000000\n",
        "",
        0,
        '--end-value and --end-time',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,'2015-01-02 12:00:00',240.0);
$dbout_create_account
COMMIT;
END
        "db1 looks good 3",
    );

    # }}}
    diag("--bv/--begin-value and --bt/--begin-time");
    testcmd("$CMD db1 --now='2015-01-01 03:00:00' --bv=13.713191 --bt='2015-01-01 03:00:00'", # {{{
        "13.713191\n",
        "",
        0,
        '--bv and --bt',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 03:00:00',13.713191,'2015-01-02 12:00:00',240.0);
$dbout_create_account
COMMIT;
END
        "db1 looks good 4",
    );

    # }}}
    testcmd("$CMD db1 --now='2015-01-01 09:00:00' --begin-value=1800 --begin-time='2015-01-02 06:00:00'", # {{{
        "7260.000000\n",
        "",
        0,
        '--begin-value and --begin-time',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-02 06:00:00',1800.0,'2015-01-02 12:00:00',240.0);
$dbout_create_account
COMMIT;
END
        "db1 looks good 5",
    );

    # }}}
    diag("Set up 1 year goal span");
    testcmd( # Set 1 year goal span {{{
        join(" ",
            $CMD,
            "db1",
            "--now='2016-01-01 00:00:00'",
            "--bt='2015-01-01 00:00:00'",
            "--bv=0",
            "--et='2016-01-01 00:00:00'",
            "--ev=31536000",
        ),
        "31536000.000000\n",
        "",
        0,
        'Set 1 year goal span',
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,'2016-01-01 00:00:00',31536000.0);
$dbout_create_account
COMMIT;
END
        "db1 looks good 6",
    );

    # }}}
    diag("Middle of year");
    testcmd("$CMD --now='2015-07-02 12:00:00' db1", # {{{
        "15768000.000000\n",
        "",
        0,
        "Goal at middle of year",
    );

    # }}}
    testcmd("$CMD --now='2015-07-02 12:00:00' db1 -v --verbose", # {{{
        "15768000.000000\n",
        <<END,
spar: currjul = '2457206.0'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '15768000'
spar: poldatediff = '0' (0d:00:00:00, 2015-07-02 12:00:00)
END
        0,
        "Goal at middle of year with -v --verbose",
    );

    # }}}
    testcmd("$CMD --time='2015-07-02 12:00:00' --now='2015-01-01' db1 -v --verbose", # {{{
        "Goal at 2015-07-02 12:00:00: 15768000.000000\n",
        <<END,
spar: currjul = '2457023.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '15768000'
spar: poldatediff = '182.5' (182d:12:00:00, 2015-07-02 12:00:00)
END
        0,
        "Goal at middle of year with --time -v --verbose",
    );

    # }}}
    diag("-c/--current");
    testcmd("$CMD --now='2015-01-02 00:00:00' -c 86400 db1 -vv", # {{{
        "86400.000000\n",
        <<END,
spar: currjul = '2457024.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '86400.0'
spar: poldatediff = '0' (0d:00:00:00, 2015-01-02 00:00:00)
END
        0,
        "-c 86400",
    );

    # }}}
    testcmd("$CMD --now='2015-01-21 00:00:00' --current 864000 db1 -vv", # {{{
        "1728000.000000\n",
        <<END,
spar: currjul = '2457043.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '864000.0'
spar: poldatediff = '-10' (-10d:00:00:00, 2015-01-11 00:00:00)
END
        0,
        "--current 864000",
    );

    # }}}
    is(sqlite_dump("db1.spar"), # {{{
        <<END,
$dbout_top
$dbout_create_config
$dbout_create_goal
INSERT INTO "goal" VALUES('2015-01-01 00:00:00',0.0,'2016-01-01 00:00:00',31536000.0);
$dbout_create_account
INSERT INTO "account" VALUES('2015-01-02 00:00:00',86400.0);
INSERT INTO "account" VALUES('2015-01-21 00:00:00',864000.0);
COMMIT;
END
        "db1 after -c 864000",
    );

    # }}}
    diag("--get-current");
    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --get-current -vv", # {{{
        join("\n",
            "10368000.000000",
            "864000.0",
            "",
        ),
        <<END,
spar: currjul = '2457143.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '864000.0'
spar: poldatediff = '-110' (-110d:00:00:00, 2015-01-11 00:00:00)
END
        0,
        "--get-current",
    );

    # }}}
    diag("Terminal support");
    my $term = "xterm";
    my $t_bold = `tput -T$term bold`;
    my $t_green = `tput -T$term setaf 2`;
    my $t_red = `tput -T$term setaf 1`;
    my $t_reset = `tput -T$term sgr0`;
    ok(length($t_bold), "Terminal supports bold");
    ok(length($t_red), "Terminal supports red");
    ok(length($t_green), "Terminal supports green");
    ok(length($t_reset), "Terminal supports reset");
    diag("-s/--status");
    testcmd("$CMD --now='2015-02-07 18:00:00' db1 -s -gvv", # {{{
        join("\n",
            "3261600.000000",
            "864000.0",
            "-2397600.000000",
            "-27d:18:00:00, 2015-01-11 00:00:00",
            "ETA 1340d:03:00:00, 2018-10-09 21:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457061.25'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '864000.0'
spar: poldatediff = '-27.75' (-27d:18:00:00, 2015-01-11 00:00:00)
spar: goal_reached = '2458401.375'
spar: goal_reached_diff = '1340.125'
END
        0,
        "-s option",
    );

    # }}}
    testcmd("TERM=$term $CMD --now='2015-02-07 18:00:00' db1 -sg --colour", # {{{
        join("\n",
            "3261600.000000",
            "864000.0",
            "$t_bold$t_red-2397600.000000$t_reset",
            "$t_bold$t_red-27d:18:00:00$t_reset, 2015-01-11 00:00:00",
            "ETA 1340d:03:00:00, 2018-10-09 21:00:00",
            "",
        ),
        "",
        0,
        "-s option with --colour, negative is red",
    );

    # }}}
    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --status --goal -vv", # {{{
        join("\n",
            "10368000.000000",
            "864000.0",
            "-9504000.000000",
            "-110d:00:00:00, 2015-01-11 00:00:00",
            "ETA 4260d:00:00:00, 2026-12-29 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457143.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '864000.0'
spar: poldatediff = '-110' (-110d:00:00:00, 2015-01-11 00:00:00)
spar: goal_reached = '2461403.5'
spar: goal_reached_diff = '4260'
END
        0,
        "--status",
    );

    # }}}
    testcmd("$CMD --now='2015-05-01 00:00:00' db1 --status -vv", # {{{
        join("\n",
            "10368000.000000",
            "864000.0",
            "-9504000.000000",
            "-110d:00:00:00, 2015-01-11 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457143.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '864000.0'
spar: poldatediff = '-110' (-110d:00:00:00, 2015-01-11 00:00:00)
END
        0,
        "--status without --goal",
    );

    # }}}
    testcmd("$CMD --now='2015-05-01 00:00:00' db1 -c 10368000 --status --goal -vv", # {{{
        join("\n",
            "10368000.000000",
            "10368000.0",
            "0.000000",
            "0d:00:00:00, 2015-05-01 00:00:00",
            "ETA 245d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457143.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '10368000.0'
spar: poldatediff = '0' (0d:00:00:00, 2015-05-01 00:00:00)
spar: goal_reached = '2457388.5'
spar: goal_reached_diff = '245'
END
        0,
        "Set current value to goal value",
    );

    # }}}
    testcmd("TERM=$term $CMD --now='2015-05-01 00:00:00' db1 -sg", # {{{
        join("\n",
            "10368000.000000",
            "10368000.0",
            "0.000000",
            "0d:00:00:00, 2015-05-01 00:00:00",
            "ETA 245d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        "",
        0,
        "Use --colour, but zero is neutral",
    );

    # }}}
    testcmd("TERM=$term $CMD --now='2015-04-30 00:00:00' db1 -sg --colour", # {{{
        join("\n",
            "10281600.000000",
            "10368000.0",
            "$t_bold${t_green}86400.000000$t_reset",
            "$t_bold${t_green}1d:00:00:00$t_reset, 2015-05-01 00:00:00",
            "ETA 242d:23:00:00, 2015-12-28 23:00:00",
            "",
        ),
        "",
        0,
        "One day earlier is green",
    );

    # }}}
    testcmd("TERM=$term $CMD --now='2015-04-30 00:00:00' db1 -sg --colour --no-colour", # {{{
        join("\n",
            "10281600.000000",
            "10368000.0",
            "86400.000000",
            "1d:00:00:00, 2015-05-01 00:00:00",
            "ETA 242d:23:00:00, 2015-12-28 23:00:00",
            "",
        ),
        "",
        0,
        "--no-colour turns colours off",
    );

    # }}}
    testcmd("$CMD --now='2016-01-01 00:00:00' db1 -c 31536000 --status --goal -vv", # {{{
        join("\n",
            "31536000.000000",
            "31536000.0",
            "0.000000",
            "0d:00:00:00, 2016-01-01 00:00:00",
            "ETA 0d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457388.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '31536000.0'
spar: poldatediff = '0' (0d:00:00:00, 2016-01-01 00:00:00)
spar: goal_reached = '2457388.5'
spar: goal_reached_diff = '0'
END
        0,
        "Goal is reached",
    );

    # }}}
    testcmd("$CMD --now='2016-01-02 00:00:00' db1 --status -vv", # {{{
        join("\n",
            "31622400.000000",
            "31536000.0",
            "-86400.000000",
            "-1d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457389.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '31536000.0'
spar: poldatediff = '-1' (-1d:00:00:00, 2016-01-01 00:00:00)
END
        0,
        "Goal is passed, current value is unchanged",
    );

    # }}}
    testcmd("$CMD --now='2016-01-02 00:00:00' -g -c 31622400 db1 --status -vv", # {{{
        join("\n",
            "31622400.000000",
            "31622400.0",
            "0.000000",
            "0d:00:00:00, 2016-01-02 00:00:00",
            "ETA -1d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457389.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '31622400.0'
spar: poldatediff = '0' (0d:00:00:00, 2016-01-02 00:00:00)
spar: goal_reached = '2457388.5'
spar: goal_reached_diff = '-1'
END
        0,
        "Goal is passed, current value is up to date",
    );

    # }}}
    diag("-w/--when");
    create_project( ('name' => 'when', 'now' => '1900-01-01 00:00:00') );
    testcmd( # Init when.spar {{{
        join(" ",
            $CMD,
            "when",
            "--now='1900-01-03 12:00:00'",
            "--bv=2",
            "--bt='1900-01-01 00:00:00'",
            "--ev=20",
            "--et='1900-01-10 00:00:00'",
        ),
        "7.000000\n",
        "",
        0,
        "Init when.spar",
    );

    # }}}
    testcmd( # Print time when current goal is 13 {{{
        join(" ",
            $CMD,
            "when",
            "--now='1900-01-02 00:00:00'",
            "--when=13",
        ),
        "4d:12:00:00, 1900-01-06 12:00:00\n",
        "",
        0,
        "Print time when current goal is 13",
    );

    # }}}
    testcmd( # Init values together with --when {{{
        join(" ",
            $CMD,
            "when",
            "--now='1800-05-27 00:00:00'",
            "--bv=10.75",
            "--bt='1800-05-10 18:00:00'",
            "--ev=29",
            "--et='1800-05-29 00:00:00'",
            "--when=28.25",
        ),
        "1d:06:00:00, 1800-05-28 06:00:00\n",
        "",
        0,
        "Init values together with --when",
    );

    # }}}
    testcmd( # Goal in the past {{{
        join(" ",
            $CMD,
            "when",
            "--now='1800-05-12 00:00:00'",
            "--bv=10.75",
            "--bt='1800-05-10 18:00:00'",
            "--ev=29",
            "--et='1800-05-29 00:00:00'",
            "--when=3.5",
        ),
        "-8d:12:00:00, 1800-05-03 12:00:00\n",
        "",
        0,
        "Goal in the past",
    );

    # }}}
    testcmd( # Negative --when {{{
        join(" ",
            $CMD,
            "when",
            "--now='1100-04-24 12:00:00'",
            "--bv=20",
            "--bt='1100-05-20 00:00:00'",
            "--ev=30",
            "--et='1100-05-30 00:00:00'",
            "--when=-5.5",
        ),
        "0d:00:00:00, 1100-04-24 12:00:00\n",
        "",
        0,
        "Negative --when",
    );

    # }}}
    ok(unlink("when.spar"), "Delete when.spar");
    create_project( ('name' => 'when-missing',
        'now' => '1900-01-01 00:00:00') );
    testcmd( # All values are missing {{{
        join(" ",
            $CMD,
            "when-missing",
            "--when=5",
        ),
        "",
        "spar: Missing begin/end time or begin/end value\n",
        1,
        "All values are missing",
    );

    # }}}
    for my $curr_invmix ("--goal", "--status", "--time='1900-01-01 12:00'") {
        testcmd( # Cannot combine --when and $curr_invmix {{{
            join(" ",
                $CMD,
                "when-missing",
                "$curr_invmix",
                "--when=5",
            ),
            "",
            "spar: Cannot combine --when with --goal, --status or --time\n",
            1,
            "Cannot combine --when and $curr_invmix",
        );
        # }}}
    }
    ok(unlink("when-missing.spar"), "Delete when-missing.spar");
    diag("-a/--add");
    testcmd("$CMD --now='2016-01-03 00:00:00' -g -a 86400 db1 --status -vv", # {{{
        join("\n",
            "31708800.000000",
            "31708800",
            "0.000000",
            "0d:00:00:00, 2016-01-03 00:00:00",
            "ETA -2d:00:00:00, 2016-01-01 00:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457390.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '31708800'
spar: poldatediff = '0' (0d:00:00:00, 2016-01-03 00:00:00)
spar: goal_reached = '2457388.5'
spar: goal_reached_diff = '-2'
END
        0,
        "-a 86400",
    );

    # }}}
    testcmd("$CMD --now='2016-01-04 00:00:00' --add -43200 db1 --status -vv", # {{{
        join("\n",
            "31795200.000000",
            "31665600",
            "-129600.000000",
            "-1d:12:00:00, 2016-01-02 12:00:00",
            "",
        ),
        <<END,
spar: currjul = '2457391.5'
spar: Goal started 2015-01-01 00:00:00 with value 0.0
spar: Goal ends at 2016-01-01 00:00:00 with value 31536000.0
spar: 86400.000000 per day, 3600.000000 per hour
spar: Current value is '31665600'
spar: poldatediff = '-1.5' (-1d:12:00:00, 2016-01-02 12:00:00)
END
        0,
        "--add with negative number",
    );

    # }}}
    diag("Goal goes down instead of up");
    create_project( ('name' => 'down', 'now' => '2003-01-01 00:00:00') );
    testcmd( # Init downwards goal {{{
        join(" ",
            $CMD,
            "down",
            "--now='2003-01-11 00:00:00'",
            "--bv=365",
            "--et='2004-01-01 00:00:00'",
            "--ev=0",
            "-c 351",
            "-sg",
            "-vv",
        ),
        "355.000000\n" .
        "351.0\n" .
        "4.000000\n" .
        "4d:00:00:00, 2003-01-15 00:00:00\n" .
        "ETA 250d:17:08:34, 2003-09-18 17:08:34\n" .
        "",
        <<END,
spar: currjul = '2452650.5'
spar: Goal started 2003-01-01 00:00:00 with value 365.0
spar: Goal ends at 2004-01-01 00:00:00 with value 0.0
spar: -1.000000 per day, -0.041667 per hour
spar: Current value is '351.0'
spar: poldatediff = '4' (4d:00:00:00, 2003-01-15 00:00:00)
spar: goal_reached = '2452901.21428571'
spar: goal_reached_diff = '250.714285714086'
END
        0,
        'Init downwards goal',
    );

    # }}}
    testcmd( # Init downwards goal {{{
        join(" ",
            $CMD,
            "--goal",
            "down",
            "--now='2003-01-12 00:00:00'",
            "-c 355.5",
            "-s",
            "-vv",
        ),
        "354.000000\n" .
        "355.5\n" .
        "-1.500000\n" .
        "-1d:12:00:00, 2003-01-10 12:00:00\n" .
        "ETA 411d:15:09:28, 2004-02-27 15:09:28\n" .
        "",
        <<END,
spar: currjul = '2452651.5'
spar: Goal started 2003-01-01 00:00:00 with value 365.0
spar: Goal ends at 2004-01-01 00:00:00 with value 0.0
spar: -1.000000 per day, -0.041667 per hour
spar: Current value is '355.5'
spar: poldatediff = '-1.5' (-1d:12:00:00, 2003-01-10 12:00:00)
spar: goal_reached = '2453063.13157895'
spar: goal_reached_diff = '411.631578947417'
END
        0,
        'Downwards and behind',
    );

    # }}}
    diag("Clean up");
    ok(unlink("db1.spar"), "Delete db1.spar");
    ok(unlink("down.spar"), "Delete down.spar");
    ok(chdir(".."), "chdir ..");
    ok(rmdir($spardir), "rmdir [spardir]");

    todo_section:
    ;

    if ($Opt{'all'} || $Opt{'todo'}) {
        diag('Running TODO tests...'); # {{{

        TODO: {

            local $TODO = '';
            # Insert TODO tests here.

        }
        # TODO tests }}}
    }

    diag('Testing finished.');
    return($Retval);
    # }}}
} # main()

sub create_project {
    # {{{
    my %in = @_;
    my $name_str = '';
    my $now_str = '';
    if (defined($in{'now'})) {
        $now_str = " --now='$in{'now'}'";
    }
    if (defined($in{'name'})) {
        $name_str = "$in{'name'}";
    }
    my $retval = testcmd("$CMD$now_str --init $name_str",
        "",
        "spar: Initialising database ./$name_str.spar\n",
        0,
        "--init $name_str",
    );
    return($retval);
    # }}}
} # create_project()

sub sql {
    # {{{
    my ($db, $sql) = @_;
    my @retval = ();

    msg(5, "sql(): db = '$db'");
    local(*CHLD_IN, *CHLD_OUT, *CHLD_ERR);

    my $pid = open3(*CHLD_IN, *CHLD_OUT, *CHLD_ERR, $SQLITE, $db) or (
        $sql_error = 1,
        msg(0, "sql(): open3() error: $!"),
        return("sql() error"),
    );
    msg(5, "sql(): sql = '$sql'");
    print(CHLD_IN "$sql\n") or msg(0, "sql(): print CHLD_IN error: $!");
    close(CHLD_IN);
    @retval = <CHLD_OUT>;
    msg(5, "sql(): retval = '" . join('|', @retval) . "'");
    my @child_stderr = <CHLD_ERR>;
    if (scalar(@child_stderr)) {
        msg(1, "$SQLITE error: " . join('', @child_stderr));
        $sql_error = 1;
    }
    return(join('', @retval));
    # }}}
} # sql()

sub sqlite_dump {
    # Return SQLite dump of database file {{{
    my $File = shift;
    my $Txt;
    if (open(my $fp, "$SQLITE $File .dump |")) {
        local $/ = undef;
        $Txt = <$fp>;
        close($fp);
        return($Txt);
    } else {
        return;
    }
    # }}}
} # sqlite_dump()

sub testcmd {
    # {{{
    my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
    defined($descriptions{$Desc}) &&
        BAIL_OUT("testcmd(): '$Desc' description is used twice");
    $descriptions{$Desc} = 1;
    my $stderr_cmd = '';
    my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
    my $Txt = join('',
        $cmd_outp_str,
        defined($Desc)
            ? $Desc
            : ''
    );
    my $TMP_STDERR = "$CMD_BASENAME-stderr.tmp";
    my $retval = 1;

    if (defined($Exp_stderr)) {
        $stderr_cmd = " 2>$TMP_STDERR";
    }
    $retval &= is(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
    my $ret_val = $?;
    if (defined($Exp_stderr)) {
        $retval &= is(file_data($TMP_STDERR), $Exp_stderr, "$Txt (stderr)");
        unlink($TMP_STDERR);
    } else {
        diag("Warning: stderr not defined for '$Txt'");
    }
    $retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");
    return($retval);
    # }}}
} # testcmd()

sub likecmd {
    # {{{
    my ($Cmd, $Exp_stdout, $Exp_stderr, $Exp_retval, $Desc) = @_;
    defined($descriptions{$Desc}) &&
        BAIL_OUT("likecmd(): '$Desc' description is used twice");
    $descriptions{$Desc} = 1;
    my $stderr_cmd = '';
    my $cmd_outp_str = $Opt{'verbose'} >= 1 ? "\"$Cmd\" - " : '';
    my $Txt = join('',
        $cmd_outp_str,
        defined($Desc)
            ? $Desc
            : ''
    );
    my $TMP_STDERR = "$CMD_BASENAME-stderr.tmp";
    my $retval = 1;

    if (defined($Exp_stderr)) {
        $stderr_cmd = " 2>$TMP_STDERR";
    }
    $retval &= like(`$Cmd$stderr_cmd`, $Exp_stdout, "$Txt (stdout)");
    my $ret_val = $?;
    if (defined($Exp_stderr)) {
        $retval &= like(file_data($TMP_STDERR), $Exp_stderr, "$Txt (stderr)");
        unlink($TMP_STDERR);
    } else {
        diag("Warning: stderr not defined for '$Txt'");
    }
    $retval &= is($ret_val >> 8, $Exp_retval, "$Txt (retval)");
    return($retval);
    # }}}
} # likecmd()

sub file_data {
    # Return file content as a string {{{
    my $File = shift;
    my $Txt;
    if (open(my $fp, '<', $File)) {
        local $/ = undef;
        $Txt = <$fp>;
        close($fp);
        return($Txt);
    } else {
        return;
    }
    # }}}
} # file_data()

sub print_version {
    # Print program version {{{
    print("$progname $VERSION\n");
    return;
    # }}}
} # print_version()

sub usage {
    # Send the help message to stdout {{{
    my $Retval = shift;

    if ($Opt{'verbose'}) {
        print("\n");
        print_version();
    }
    print(<<"END");

Usage: $progname [options] [file [files [...]]]

Contains tests for the $CMD_BASENAME(1) program.

Options:

  -a, --all
    Run all tests, also TODOs.
  -h, --help
    Show this help.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -t, --todo
    Run only the TODO tests.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  --version
    Print version information.

END
    exit($Retval);
    # }}}
} # usage()

sub msg {
    # Print a status message to stderr based on verbosity level {{{
    my ($verbose_level, $Txt) = @_;

    if ($Opt{'verbose'} >= $verbose_level) {
        print(STDERR "$progname: $Txt\n");
    }
    return;
    # }}}
} # msg()

__END__

# This program is free software; you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 2 of the License, or (at 
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program.
# If not, see L<http://www.gnu.org/licenses/>.

# vim: set fenc=UTF-8 ft=perl fdm=marker ts=4 sw=4 sts=4 et fo+=w :
