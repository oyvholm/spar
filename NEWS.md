Summary of changes in Spar
==========================

For a complete log of changes, please refer to the Git commit log in the 
repositories mentioned in `README.md`.

v0.x.x - 201x-xx-xx
-------------------

- Add the `-w`/`--when` option, find out when the expected goal reaches 
  a specific value.
- Add the `--colour` and `--no-colour` options.
- spar.t: Be more flexible about version strings. `filesynced --patch`, 
  apply commit c4cf639 ("Lib/std/perl-tests: Allow everything after 
  x.y.z in the version number", 2016-04-27) from `sunny256/utils.git`.

v0.3.0 - 2015-12-24
-------------------

- Use minus sign instead of "ahead"/"behind"/"right on"/"ago" messages 
  in output. Negative value is behind schedule, positive ahead.
- Add `-g`/`--goal` option, display estimated time the goal will be 
  reached.
- Create the `-a`/`--add` option for adding or subtracting values 
  to/from the current value in the database.
- Remove Perl warnings when begin time, begin value, end time or end 
  value weren't defined.
- Fix error in message if goal value is lower than start value, "behind" 
  and "ahead" were switched.
- Display time and interval until/since goal is/was passed.
- Don't allow whitespace in project names.
- If `--init` isn't used, abort immediately if project doesn't exist to 
  avoid that `sqlite3` creates empty files.
- Create a `config` table in the database:
  - Contains the entry `dbversion` with value `1` which is the current 
    version of the database schema. If any incompatible changes are 
    made, it's incremented and some way of upgrading the database will 
    be provided.
  - Read `dbversion` when accessing the database. If the value is blank, 
    create the `config` table. Abort if sqlite3 returns error.
  - Refuse to run if the database version is greater than the current 
    version.
- Increase verbose level for SQL messages from 3 (`-vvv`) to 5 
  (`-vvvvv`).
- Remove stray apostrophe from verbose output.
- Clean up output from `t/spar.t`, remove the command executed in the 
  test.
- "make test" redirects stderr to stdout to make it easier to grep the 
  output.

v0.2.0 - 2015-12-14
-------------------

- Add these options:
  - `-c`/`--current`
  - `-d`/`--directory`
  - `--get-current`
  - `--now`
  - `-s`/`--status`
- Add tests for all current functionality in `t/spar.t`.
- Display necessary value change per day and hour if `-v` is specified.
- Suppress sqlite3 warnings, enable with `-v`.

v0.1.0 - 2015-12-12
-------------------

- Use Semantic Versioning as described at <http://semver.org>.
- Create and use SQLite files in `~/spar/`, extract value as it is right 
  now or at a specified time using the `-t`/`--time` option.
- Add the following options:
  - `--begin-time` TIME, `--bt` TIME
  - `--begin-value` VAL, `--bv` VAL
  - `--end-time` TIME, `--et` TIME
  - `--end-value` VAL, `--ev` VAL
  - `-h`, `--help`
  - `--init` DBNAME
  - `-q`, `--quiet`
  - `-t` TIME, `--time` TIME
  - `-v`, `--verbose`
  - `--version`
- Add tests for the standard options in `t/spar.t`.
- Add `README.md`.
- Add `synced.sql` and sync info to `Makefile`.

----

    File ID: 1d3a0150-a061-11e5-b626-02010e0a6634
    vim: set tw=72 ts=2 sw=2 sts=2 fo=tcqw fenc=utf8 :
