# spar.git/Makefile
# File ID: d78ed1a0-80d8-11e5-85e8-02010e0a6634

default:
	@echo No default action for make >&2

clean:
	rm -fv synced.sqlite.*.bck
	cd t && make clean

test:
	cd t && make
