# spar.git/Makefile
# File ID: d78ed1a0-80d8-11e5-85e8-02010e0a6634

.PHONY: default
default:

.PHONY: clean
clean:
	rm -f synced.sqlite.*.bck
	cd t && $(MAKE) clean

.PHONY: test
test:
	cd t && $(MAKE) test
