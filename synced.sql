PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE synced (
  file TEXT
    CONSTRAINT synced_file_length
      CHECK (length(file) > 0)
    UNIQUE
    NOT NULL,
  orig TEXT,
  rev TEXT
    CONSTRAINT synced_rev_length
      CHECK (length(rev) = 40 OR rev = ''),
  date TEXT
    CONSTRAINT synced_date_length
      CHECK (date IS NULL OR length(date) = 19)
    CONSTRAINT synced_date_valid
      CHECK (date IS NULL OR datetime(date) IS NOT NULL)
);
INSERT INTO synced VALUES('COPYING','http://www.gnu.org/licenses/gpl-2.0.txt','','2015-12-12 00:22:05');
INSERT INTO synced VALUES('spar','Lib/std/perl','a0640fc5413ef330e24bc54f40c749a9cdbadb8b','2015-11-01 20:37:20');
INSERT INTO synced VALUES('t/spar.t','Lib/std/perl-tests','2e2c99b8b3e426ba7ce8b433584b359ba5e738eb','2016-04-28 02:02:08');
CREATE TABLE todo (
  file TEXT
    CONSTRAINT todo_file_length
      CHECK(length(file) > 0)
    UNIQUE
    NOT NULL
  ,
  pri INTEGER
    CONSTRAINT todo_pri_range
      CHECK(pri BETWEEN 1 AND 5)
  ,
  comment TEXT
);
COMMIT;
