README for Spar
===============

Source code
-----------

Spar can be cloned from the following repositories:

- `https://gitlab.com/sunny256/spar.git` (Main repo)
- `git://github.com/sunny256/spar.git`

Git branches
------------

The `master` branch is considered stable and will never be rebased. 
Every new functionality or bug fix is created on topic branches which 
may be rebased now and then. All tests on `master` (executed with "make 
test") should succeed. If any test fails, it’s considered a bug and 
should be reported in the issue tracker.

Version compatibility
---------------------

To ensure compatibility between versions, the program follows the 
Semantic Versioning Specification described at <http://semver.org>. 
Using the version number `X.Y.Z` as an example:

- `X` is the *major version*. This number is only incremented when 
  backwards-incompatible changes are introduced.
- `Y` is the *minor version*. Increased when new backwards-compatible 
  features are added.
- `Z` is the *patch level*. Increased when new backwards-compatible 
  bugfixes are added.

Bugs and suggestions
--------------------

Bugs and suggestions can be filed in the issue tracker at 
<https://gitlab.com/sunny256/spar/issues>.

License
-------

Author: Øyvind A. Holm <sunny@sunbase.org>

License: GNU General Public License version 2 or later, see the file 
`COPYING` for more info.

This program is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the 
Free Software Foundation, either version 2 of the License, or (at your 
option) any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
Public License for more details.

You should have received a copy of the GNU General Public License along 
with this program. If not, see 
<http://www.gnu.org/licenses/gpl-2.0.txt>.

    File ID: d037aa98-a064-11e5-b414-02010e0a6634
    vim: set ft=markdown tw=72 fenc=utf8 et ts=2 sw=2 sts=2 fo=tcqw :
